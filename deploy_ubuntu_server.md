# Deploy Ubuntu Server

| [simJOP/docs ](https://gitlab.com/simjop/docs#simjopdocs) |

Content of this part:

[[_TOC_]]

## Ubuntu server

Those steps are tested on Ubuntu 20.04 LTS.

### Packages

    sudo apt update && sudo apt upgrade -y
    sudo apt install -y vim mc git tig htop iotop net-tools

### Hostname

    hostnamectl set-hostname <domain>

### Default editor

    sudo update-alternatives --config editor

### Unprivileged user with sudo

    passwd root

    adduser <user>
    usermod -aG sudo <user>

    # copy ssh key from from LOCAL machine
    ssh-copy-id <user@machine>

    # advanced configuration on LOCAL machine
    $ cat ~/.ssh/config
    Host <host>
        HostName <domain>
        Port <port_number>
        User <user>

    # sshd hardening
    vim /etc/ssh/sshd_config

    $ cat /etc/ssh/sshd_config | grep PermitRootLogin
    PermitRootLogin prohibit-password
    $ cat /etc/ssh/sshd_config | grep PasswordAuthentication
    PasswordAuthentication no
    $ cat /etc/ssh/sshd_config | grep PubkeyAuthentication
    PubkeyAuthentication yes

    systemctl restart sshd.service
    systemctl status sshd.service

## Red root prompt

    # cat /root/.bashrc
    # [...]
    force_color_prompt=yes
    # [...]
    if [ "$color_prompt" = yes ]; then
        PS1='${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
    # [...]

## Firewall

    vim /etc/ssh/sshd_config
    $ cat /etc/ssh/sshd_config | grep Port
    Port <port_number>

    sudo apt install firewalld

    systemctl enable firewalld.service
    systemctl start firewalld.service

    # ssh
    firewall-cmd --permanent --service="ssh" --add-port "<port_number>/tcp"
    firewall-cmd --permanent --service="ssh" --remove-port "22/tcp"

    # simjop_server
    firewall-cmd --permanent --service="http" --add-port "80/tcp"
    firewall-cmd --permanent --service="https" --add-port "443/tcp"

    firewall-cmd --reload
    systemctl reload sshd.service

    # check open ports
    sudo netstat -ltup
    sudo netstat -lntup
