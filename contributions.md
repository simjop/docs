# Contributions

| [simJOP/docs ](https://gitlab.com/simjop/docs#simjopdocs) |

Content of this part:

[[_TOC_]]

## Setup local git repositories

SimJOP project contains many git repositories. We ship three scripts for your convenience:
* clone_all.sh -- downloads and setup project git repos locally
* status.sh -- shows which repo contains new work
* local_build.sh -- prepare local "build" of the project

So, if you want to clone and configure all repositories at once, please do:

    # download clone script
    mkdir simjop
    cd simjop
    wget https://gitlab.com/simjop/docs/-/raw/master/contrib/clone_all.sh

    # edit necessary values
    vim clone_all.sh

    # run the script
    bash clone_all.sh

    # remove clone script because it is dangerous
    rm clone_all.sh

## Local development

Rapid development needs quick debug iterations.

### Packages

    sudo dnf install -y nodejs npm mariadb mariadb-server redis

### Setup

    # MariaDB
    sudo systemctl start mariadb
    sudo mysql_secure_installation

    sudo mysql -u root -p
    CREATE DATABASE simjop_db;
    CREATE USER 'john'@'localhost' IDENTIFIED BY 'pass1234';
    GRANT ALL ON simjop_db.* TO 'john'@'localhost';

    # Redis
    sudo vim /etc/redis/redis.conf

    # NodeJS based projects
    npm install --save-dev npm-check-updates
