# simJOP/docs

This repository is dedicated to documentation of whole simJOP project.

Content of this part:

[[_TOC_]]

## Content

### For developers

Right now, there is no easy way how to run GitLab Runner based on Fedora environment with Podman. So please, install Ubuntu server and run Docker environment.

- [Contributions](https://gitlab.com/simjop/docs/-/blob/master/contributions.md#contributions)
- [Architecture](https://gitlab.com/simjop/docs/-/blob/master/architecture.md#architecture)
- [Deploy Ubuntu Server](https://gitlab.com/simjop/docs/-/blob/master/deploy_ubuntu_server.md)
- [Deploy Fedora Server](https://gitlab.com/simjop/docs/-/blob/master/deploy_fedora_server.md)
- [Deploy GitLab Runner](https://gitlab.com/simjop/docs/-/blob/master/deploy_gitlab_runner.md)

### Resources

#### General
- [regular expression](https://regexr.com/)
- [yaml](https://pyyaml.org/wiki/PyYAMLDocumentation)

#### Docker
- [Multiple virtual hosts with SSL on one machine](https://dolphinandmermaids.com/blog/docker-nginx-letsencrypt)
- [nginx-proxy](https://hub.docker.com/r/jwilder/nginx-proxy)
- [letsencrypt-nginx-proxy-companion](https://hub.docker.com/r/jrcs/letsencrypt-nginx-proxy-companion)
- [mariadb](https://hub.docker.com/_/mariadb)
- [adminer](https://hub.docker.com/_/adminer/)

#### Python
- [eventlet](http://eventlet.net/) -- not used
- [python-socketio](https://python-socketio.readthedocs.io/en/latest/index.html)
- [python-socketio tutorial](https://tutorialedge.net/python/python-socket-io-tutorial/)
- [aiohttp](https://docs.aiohttp.org/en/stable/)
- [aiohttp-oauth2](https://github.com/mattrasband/aiohttp-oauth2) -- just inspiration
- [async SQLAlchemy](https://docs.sqlalchemy.org/en/14/orm/extensions/asyncio.html)

#### Node.js
- [node.js](https://nodejs.dev/learn)
- [socket.io](https://socket.io/docs/v4/)

#### Discord
- [Discord OAuth2](https://discord.com/developers/docs/topics/oauth2)
- [Discord API](https://discord.com/developers/docs/reference)

#### React
- [react](https://reactjs.org/docs/getting-started.html)
- [electron & react](https://www.section.io/engineering-education/desktop-application-with-react/)
- [bootstrap](https://www.bootstrapcdn.com/)
- [react-bootstrap](https://react-bootstrap.github.io/getting-started/introduction/)
- [konva](https://konvajs.org/api/Konva.html)
- [react and canvas](https://lavrton.com/using-react-with-html5-canvas-871d07d8d753/)
- [react events](https://reactjs.org/docs/handling-events.html)
- [mouse event](https://stackoverflow.com/questions/31519758/reacts-mouseevent-doesnt-have-offsetx-offsety)
