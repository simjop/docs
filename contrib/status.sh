#!/usr/bin/env bash

# Obtain the path to the work directory
RELATIVE_SOURCE_PATH=`dirname ${BASH_SOURCE[@]}`
SOURCE_PATH=`readlink -f ${RELATIVE_SOURCE_PATH}`

for f in ${SOURCE_PATH}/*; do
    if [ -d "$f" ]; then
        echo "===== $f ====="
        git -C $f status
    fi
done
