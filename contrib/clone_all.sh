#!/usr/bin/env bash

NAME=""
EMAIL=""
GPG_KEY=""


# Obtain the path to the work directory
RELATIVE_SOURCE_PATH=`dirname ${BASH_SOURCE[@]}`
SOURCE_PATH=`readlink -f ${RELATIVE_SOURCE_PATH}`

git clone git@gitlab.com:simjop/docs.git
git clone git@gitlab.com:simjop/build_image.git
git clone git@gitlab.com:simjop/infra.git
git clone git@gitlab.com:simjop/web.git
git clone git@gitlab.com:simjop/server.git
git clone git@gitlab.com:simjop/editor.git
git clone git@gitlab.com:simjop/client.git


for f in ${SOURCE_PATH}/*; do
    if [ -d "$f" ]; then
        echo "===== $f ====="
        git -C $f config user.name ${NAME}
        git -C $f config user.email ${EMAIL}
        git -C $f config user.signingkey ${GPG_KEY}
        git -C $f config core.editor "vim"
        git -C $f config commit.template .git-commit-template
        git -C $f config commit.verbose true
        git -C $f config pull.ff only
        git -C $f config fetch.prune true
        git -C $f config diff.colorMoved zebra
        git -C $f config -l
    fi
done

cp ${SOURCE_PATH}/docs/contrib/status.sh ${SOURCE_PATH}/
cp ${SOURCE_PATH}/docs/contrib/local_build.sh ${SOURCE_PATH}/
