#!/usr/bin/env bash

# Obtain the path to the work directory
RELATIVE_SOURCE_PATH=`dirname ${BASH_SOURCE[@]}`
SOURCE_PATH=`readlink -f ${RELATIVE_SOURCE_PATH}`

LOCAL_BUILD="${SOURCE_PATH}/.local_build"


func_update () {
    cd ${SOURCE_PATH}/editor
    npm outdated
    npx ncu -u
    npm update
    npm install
    git diff package.json
}

func_build_all () {
    if [ -d "${LOCAL_BUILD}" ]; then
        rm -Rf ${LOCAL_BUILD};
    fi

    # build app
    npm run build --prefix ${SOURCE_PATH}/editor

    # build web
    cd ${SOURCE_PATH}/web
    python3 -m venv env
    source env/bin/activate
    pip3 install -r requirements.txt
    deactivate
    cd ${SOURCE_PATH}

    # copy all
    mkdir -p ${LOCAL_BUILD}
    cp -r ${SOURCE_PATH}/web/env/ ${LOCAL_BUILD}
    cp -r ${SOURCE_PATH}/web/simjop_web/ ${LOCAL_BUILD}
    cp ${SOURCE_PATH}/web/simjop_web.cfg ${LOCAL_BUILD}
    cp -r ${SOURCE_PATH}/editor/build/ ${LOCAL_BUILD}/simjop_web/app
}

func_build_app () {
    if [ -d "${LOCAL_BUILD}" ]; then
        rm -Rf ${LOCAL_BUILD};
    fi

    # build app
    npm run build --prefix ${SOURCE_PATH}/editor

    # copy all
    mkdir -p ${LOCAL_BUILD}
    cp -r ${SOURCE_PATH}/web/env/ ${LOCAL_BUILD}
    cp -r ${SOURCE_PATH}/web/simjop_web/ ${LOCAL_BUILD}
    cp ${SOURCE_PATH}/web/simjop_web.cfg ${LOCAL_BUILD}
    cp -r  ${SOURCE_PATH}/editor/build/ ${LOCAL_BUILD}/simjop_web/app
}

func_build_web () {
    if [ -d "${LOCAL_BUILD}" ]; then
        rm -Rf ${LOCAL_BUILD};
    fi

    # build web
    cd ${SOURCE_PATH}/web
    python3 -m venv env
    source env/bin/activate
    pip3 install -r requirements.txt
    deactivate
    cd ${SOURCE_PATH}

    # copy all
    mkdir -p ${LOCAL_BUILD}
    cp -r ${SOURCE_PATH}/web/env/ ${LOCAL_BUILD}
    cp -r ${SOURCE_PATH}/web/simjop_web/ ${LOCAL_BUILD}
    cp ${SOURCE_PATH}/web/simjop_web.cfg ${LOCAL_BUILD}
    cp -r ${SOURCE_PATH}/editor/build/ ${LOCAL_BUILD}/simjop_web/app
}

func_run () {
    cd ${LOCAL_BUILD}
    source ./env/bin/activate
    python simjop_web run
}


if [ "$1" == "" ]; then
    echo "./local_build.sh (update|build|build app|build web|run)"
elif [ "$1" == "update" ]; then
    func_update
elif [ "$1" == "build" ]; then
    if [ "$2" == "" ]; then
        func_build_all
    elif [ "$2" == "app" ]; then
        func_build_app
    elif [ "$2" == "web" ]; then
        func_build_web
    fi
elif [ "$1" == "run" ]; then
    func_run
fi
