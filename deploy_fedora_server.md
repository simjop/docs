# Deploy Fedora Server

| [simJOP/docs ](https://gitlab.com/simjop/docs#simjopdocs) |

Content of this part:

[[_TOC_]]

## Fedora server

Those steps are tested on Fedora 31 (update to 35 needed 35).

### Fastest repository mirror

    echo "max_parallel_downloads=10" >> /etc/dnf/dnf.conf
    echo "deltarpm=true" >> /etc/dnf/dnf.conf
    echo "fastestmirror=true" >> /etc/dnf/dnf.conf
    dnf -y update

### Utils

    dnf install -y --allowerasing vim-enhanced vim-default-editor mc git tig htop iotop

### Hostname

    hostnamectl set-hostname <domain>

### Unprivileged user with sudo

    passwd root

    adduser <user>
    passwd <user>
    usermod -aG wheel <user>

    # copy ssh key from from LOCAL machine
    ssh-copy-id <user@machine>

    # advanced configuration on LOCAL machine
    $ cat ~/.ssh/config
    Host <host>
        HostName <domain>
        Port <port_number>
        User <user>

    # set no password for sudo command if needed
    # ssh <user>
    # sudo visudo

    # sshd hardening
    vim /etc/ssh/sshd_config

    $ cat /etc/ssh/sshd_config | grep PermitRootLogin
    PermitRootLogin no
    $ cat /etc/ssh/sshd_config | grep PasswordAuthentication
    PasswordAuthentication no
    $ cat /etc/ssh/sshd_config | grep PubkeyAuthentication
    PubkeyAuthentication yes

    systemctl restart sshd.service
    systemctl status sshd.service

## Firewall

    vim /etc/ssh/sshd_config
    $ cat /etc/ssh/sshd_config | grep Port
    Port <port_number>

    semanage port -a -t ssh_port_t -p tcp <port_number>
    semanage port -l | grep ssh_port_t


    systemctl enable firewalld.service
    systemctl start firewalld.service
    firewall-cmd --permanent --service="ssh" --add-port "<port_number>/tcp"

    firewall-cmd --reload
    systemctl reload sshd

    firewall-cmd --permanent --service="ssh" --remove-port "22/tcp"
    firewall-cmd --reload

    # check open ports
    sudo netstat -ltup
    sudo netstat -lntup

## Notes

    # VNC client
    sudo dnf install vinagre

    # generate ssh key
    ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
