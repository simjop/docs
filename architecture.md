# Architecture

| [simJOP/docs ](https://gitlab.com/simjop/docs#simjopdocs) |

Content of this part:

[[_TOC_]]

## Ports

| port | service  | application | url       |
| :--- | :---     | :---        | :---      |
| 80   | http     | web         | simjop.cz |
| 443  | https    | web         | simjop.cz |


web
server
client
editor
db
db-admin

