# Deploy GitLab Runner

| [simJOP/docs ](https://gitlab.com/simjop/docs#simjopdocs) |

Content of this part:

[[_TOC_]]

This manual is tested on Ubuntu 20.04 LTS.

## Install Docker

    sudo apt-get update
    sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    # check
    sudo apt-key fingerprint 0EBFCD88

    sudo add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) \
        stable"

    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli docker-compose containerd.io

    sudo docker run hello-world

## Install GitLab Runner

    curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
    sudo apt-get install gitlab-runner

    cat <<EOF | sudo tee /etc/apt/preferences.d/pin-gitlab-runner.pref
    Explanation: Prefer GitLab provided packages over the Debian native ones
    Package: gitlab-runner
    Pin: origin packages.gitlab.com
    Pin-Priority: 1001
    EOF

## Register GitLab Runners

Follow https://docs.gitlab.com/runner/register/index.html

    cp infra/template/gitlab_runner.env ~/.env/gitlab_runner.env
    vim ~/.env/gitlab_runner.env
    source ~/.env/gitlab_runner.env

    sudo gitlab-runner register -n \
        --url https://gitlab.com/ \
        --registration-token $GITLAB_RUNNER_REGISTRATION_TOKEN \
        --executor docker \
        --description "simjop runner" \
        --docker-image "docker" \
        --docker-privileged \
        --docker-volumes "/certs/client"
